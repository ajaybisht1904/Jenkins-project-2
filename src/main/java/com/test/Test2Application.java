package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Test2Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2Application.class, args);
	}
	
	@GetMapping("")
	public String test() {
		return "test 2 success!!";
	}
	
	@GetMapping("/main")
	public String main() {
		return "main fn. success!!";
	}
	@GetMapping("/dev1")
	public String test2() {
		return "dev1 test success!!";
	}

	@GetMapping("/dev1/demo")
	public String dev1() {
		return "dev1/demo test success!!";
	}
	
}
